#include <stdio.h>
/* Copie l'entrée sur la sortie (version 2) */
int main()
{
	char c; // Sur Linux ARM, remplacer "char" par "signed char"
	while ( (c = getchar()) != EOF )
		putchar(c);
	return 0;
}
