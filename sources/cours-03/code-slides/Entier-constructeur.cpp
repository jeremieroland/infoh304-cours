#include <iostream>
using std::cout;
using std::endl;

class Entier {
public:
	Entier() {
		cout << "Construction d'un Entier" << endl;
	}
	int val;
};

int main() {
	Entier* O1 = new Entier();
	Entier O2;
	/* Utilisation des entiers */
	delete O1;
}