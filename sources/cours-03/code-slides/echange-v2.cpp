#include <iostream>
using namespace std;

/* echange: échange *pa et *pb */
void echange(int* pa, int* pb)
{
    int temp = *pa;
    *pa = *pb;
    *pb = temp;
}

int main()
{
    int x = 1, y = 2;
    echange(&x, &y);
    cout << "x = " << x;
    cout << ", y = " << y << endl;
    return 0;
}