#include <iostream>
using std::cout;
using std::endl;

class Entier {
private:
	int val;
public:
	Entier( int valInit = 0 ) : val(valInit) {}
	int getVal() const
		{ return val; }
	void setVal( int nouvVal )
		{ val = nouvVal; }
};

int main() {
	Entier* O1 = new Entier( 3 );
	cout << "O1 vaut " << O1->getVal() << endl;
	Entier O2;
	O2.setVal( 5 );
	cout << "O2 vaut " << O2.getVal() << endl;
	delete O1;
}
