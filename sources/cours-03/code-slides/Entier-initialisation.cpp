#include <iostream>
using std::cout;
using std::endl;

class Entier {
public:
	Entier( int valInit = 0 ) : val(valInit) {
		cout << "Construction d'un Entier de valeur " << valInit << endl;
	}
	int val;
};

int main() {
	Entier* O1 = new Entier( 3 );
	Entier O2( 5 );
	Entier O3;
	/* Utilisation des entiers */
	delete O1;
}