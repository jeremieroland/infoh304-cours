#include <iostream>
using std::cout;
using std::endl;
using std::ostream;

template <typename NomDuType>
class Nombre {
private:
	NomDuType valeur;
public:
	Nombre( NomDuType valeurInitiale = 0) : valeur( valeurInitiale ) {}
	void print ( ostream & out = cout ) const
		{ out << valeur; }
	bool operator< ( const Nombre & NombreDroite ) const
		{ return valeur < NombreDroite.valeur; }
	// ...
};

int main() {
	Nombre<int> entier (5);
	entier.print(); cout << endl;
	
	Nombre<float> reel (3.14);
	reel.print(); cout << endl;
	return 0;
}
