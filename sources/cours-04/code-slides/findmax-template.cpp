#include <vector>
#include <iostream>
using std::vector;
using std::cout;
using std::endl;

template <typename NomDuType>
NomDuType findMax ( const vector<NomDuType> & tableau )
{
	int indiceMax = 0;
	for ( int i = 1; i < tableau.size(); i++)
		if ( tableau[ indiceMax ] < tableau[ i ] )
			indiceMax = i;
	return tableau[ indiceMax ];
}

template <typename Container>
void print( const Container & c )
{
	typename Container::const_iterator itr;
	for ( itr = c.begin(); itr != c.end(); ++itr )
		cout << *itr << " ";
	cout << endl;
}

int main () {
	int tab[] = {1, 5, 3, 7, 4};
	vector<int> entiers(tab,tab+5);
	print(entiers);
	cout << "max = " << findMax(entiers) << endl;
	
	float tab2[] = {0.5, 5.9, 3.2, 4.8, 1.6};
	vector<float> reels(tab2,tab2+5);
	print(reels);
	cout << "max = " << findMax(reels) << endl;
	
	long tab3[] = {15453, 6934, 234543, 3834, 2643543};
	vector<long> longs(tab3,tab3+5);
	print(longs);
	cout << "max = " << findMax(longs) << endl;
	
	return 0;  
}