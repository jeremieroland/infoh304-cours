#include <iostream>
#include <string>
using std::cout;
using std::endl;
using std::ostream;
using std::string;

class MembreULB {
	string prenom;
	string nom;
public:
	int matricule;
	MembreULB(string nomInit="", string prenomInit="");
	virtual ~MembreULB();
	string getPrenom() const;
	void setPrenom(string nouveauPrenom);
	string getNom() const;
	void setNom(string nouveauNom);
	virtual void print(ostream & out = cout ) const;
};

class EtudiantULB : virtual public MembreULB {
	string cursus;
public:
	EtudiantULB(string nomInit="", string prenomInit="", string cursusInit="");
	virtual ~EtudiantULB();
	string getCursus() const;
	void setCursus(string nouveauCursus);
	virtual void print(ostream & out = cout ) const;
};

class EmployeULB : virtual public MembreULB {
	string fonction;
public:
	EmployeULB(string nomInit="", string prenomInit="", string fonctionInit="");
	virtual ~EmployeULB();
	string getFonction() const;
	void setFonction(string nouvelleFonction);
	virtual void print(ostream & out = cout ) const;
};

class EmployeEtudiantULB : public EtudiantULB, public EmployeULB {
public:
	EmployeEtudiantULB(string nomInit="", string prenomInit="", string cursusInit="", string fonctionInit="");
	~EmployeEtudiantULB();
	void print(ostream & out = cout ) const;
};
