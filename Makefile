PREAMBLE = sources/common/preamble.tex
SOURCES = $(wildcard sources/cours-??)
TARGETS = $(patsubst sources/%,%,$(SOURCES))
PRECIOUS = $(addsuffix /slides.pdf,$(SOURCES)) $(addsuffix /slides-animated.pdf,$(SOURCES)) $(addsuffix /figures/externalize,$(SOURCES))

.PRECIOUS: $(PRECIOUS)
.PHONY: animated
all: $(TARGETS)
clean:
	for directory in $(SOURCES); do cd $${directory} &&latexmk -c -r ../common/latexmkrc && latexmk -f -c -r ../common/latexmkrc slides-animated.tex && cd ../.. ; done
cleaner:
	for directory in $(SOURCES); do cd $${directory} &&latexmk -C -r ../common/latexmkrc && latexmk -f -C -r ../common/latexmkrc slides-animated.tex && rm -rf figures/externalize && cd ../.. ; done
.SECONDEXPANSION:
cours-%: sources/cours-%/slides.pdf sources/cours-%/README.md
	rm -rf cours-$*
	mkdir cours-$*
	cp sources/cours-$*/slides.pdf cours-$*/cours-$*-slides.pdf
	cp sources/cours-$*/README.md cours-$*/README.md
	if [ -d sources/cours-$*/code ]; then \
	cp -R sources/cours-$*/code cours-$*/code; fi
cours-%-animated: sources/cours-%/slides-animated.pdf
	mkdir -p cours-$*
	cp sources/cours-$*/slides-animated.pdf cours-$*/cours-$*-slides-animated.pdf
sources/cours-%/slides.pdf: sources/cours-%/slides.tex $(PREAMBLE) $$(wildcard sources/cours-%/code/*) $$(wildcard sources/cours-%/code-slides/*) $$(wildcard sources/cours-%/figures/*) sources/cours-%/figures/externalize
	latexmk -pdf -cd -synctex=1 -interaction=nonstopmode -shell-escape -silent $<
sources/cours-%/figures/externalize:
	mkdir -p sources/cours-$*/figures/externalize
animated: $(addsuffix -animated,$(TARGETS))
sources/cours-%/slides-animated.pdf: sources/cours-%/slides.tex $(PREAMBLE) $$(wildcard sources/cours-%/code/*) $$(wildcard sources/cours-%/code-slides/*) $$(wildcard sources/cours-%/figures/*) sources/cours-%/figures/externalize
	echo "\def\Animated{}" > sources/cours-$*/version.tex
	ln -s slides.tex sources/cours-$*/slides-animated.tex
	latexmk -pdf -cd -synctex=1 -interaction=nonstopmode -shell-escape -jobname="slides-animated" -silent $<
	rm -rf sources/cours-$*/version.tex
	rm -rf sources/cours-$*/slides-animated.tex
