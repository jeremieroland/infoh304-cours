# INFOH304-cours
Ce dépôt reprend toutes les sources LaTeX et exemples de code pour le cours INFOH304. Les fichiers LaTeX pour les slides peuvent être compilés manuellement ou via le fichier Makefile fourni. La compilation nécessite une version récente de TeX Live (testé avec la version 2021). En cas de souci de compilation, vérifiez les erreurs pour voir s'il ne vous manque pas un paquet annexe, pour les détails voir ci-dessous.
## Compilation avec Makefile
Instructions pour chaque cible:
### `make` ou `make all`
Compile les slides pour tous les cours et place les fichiers PDF produits (ainsi que les exemples de code pour les cours 01 à 04) dans les dossiers cours-XX.
### `make cours-XX`
Idem que `make all` mais uniquement pour le cours XX (où XX est compris entre 01 et 12)
### `make animated`
Crée une version des slides avec animations, telle qu'utilisée pendant le cours en direct (ne pas utiliser cette version pour l'impression)
### `make cours-XX-animated`
Idem que `make animated` mais uniquement pour le cours XX (où XX est compris entre 01 et 12)
### `make clean`
Supprime la plupart des fichiers auxiliaires créés par LaTeX lors de la compilation, sauf les images temporaires. Après un `make clean`, un appel à `make` ne relancera pas de compilation sauf si un des fichiers sources a été modifié
### `make cleaner`
Supprime tous les fichiers auxiliaires créés par LaTeX lors de la compilation, y compris les images temporaires. Après un `make cleaner`, un appel à `make` relancera la compilation de tous les fichiers (ce qui peut être long en raison des nombreuses images à générer)
## Compilation manuelle
Pour compiler les fichiers à la main, utiliser la commande suivante (notez les options de compilation):
> pdflatex -synctex=1 -interaction=nonstopmode -shell-escape slides.tex

Pour la génération des images temporaires, cette commande nécessite l'existence d'un sous-dossier `figures/externalize`, et la présence du fichier `preamble.tex` dans le dossier supérieur
